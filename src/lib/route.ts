import type {Context} from "@arabesque/listener-http";
import type {Middleware} from "@arabesque/core";
import {parse} from "url";
import * as RouteParser from "route-parser";
import type {HTTPMethod} from "./http-method";
import {METHODS} from "http";

export type RouteHandler<Context, Parameters extends Record<string, any>> = (
    context: Context,
    parameters: Parameters
) => Promise<void>;

const isAnHTTPMethod = (candidate: string): candidate is HTTPMethod => {
    return METHODS.includes(candidate);
};

export const createRoute = <C extends Context, P extends Record<string, string>>(
    method: HTTPMethod,
    specification: string,
    handler: RouteHandler<C, P>
): Middleware<C> => {
    return (context, next) => {
        const {message} = context;

        if (message.url && context.message.method && isAnHTTPMethod(context.message.method) && (context.message.method === method)) {
            const {pathname} = parse(message.url);

            if (pathname !== null) {
                const routeParser = new RouteParser<P>(specification);
                const parameters = routeParser.match(pathname) as P | false;

                if (parameters !== false) {
                    return handler(context, parameters)
                        .then(() => {
                            return next(context);
                        });
                }
            }
        }

        return Promise.resolve(context);
    };
};