import type {Middleware} from "@arabesque/core";
import type {Context} from "@arabesque/listener-http";
import {createORMiddleware} from "@arabesque/logic-middlewares";

export const createRouter = <C extends Context>(
    ...routes: Array<Middleware<C>>
): Middleware<C> => {
    return createORMiddleware(...routes);
};